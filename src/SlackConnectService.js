import { ApplicationService } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { AccountReplacer } from './AccountReplacer';
import { InstantMessageReplacer } from "./InstantMessageReplacer";
export class SlackConnectService extends ApplicationService {
    constructor(app) {

        new AccountReplacer(app).apply();

        new InstantMessageReplacer(app).apply();

        super(app);
        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
        }
    }

}